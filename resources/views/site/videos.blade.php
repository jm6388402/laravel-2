@extends('layouts.site')

@section('titulo','javascripts')

@section('conteudo')

<h1 id="video">VIDEOS AULA</h1>
<div class="container">
    <div class="row">
        <div class="col-5">
                <iframe width="350" height="200" src="https://www.youtube.com/embed/iZ1ucWosOww" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-7" >
            <h3 class="html">Curso de HTML e CSS para iniciantes Aula </h3>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Hic, nihil. Officiis sit consectetur earum dolorum rerum dolores veritatis laboriosam commodi labore, nesciunt magni illum quod exercitationem magnam possimus porro vero?</p>
        </div>
    </div>
</div>
<div class="container">
    <div class="row ">
        <div class="col-lg-4 col-md-4 mt-3">
                <iframe width="250" height="200" src="https://www.youtube.com/embed/P4BNi_yPehc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <p><strong>Lorem ipsum dolor sit amet consectetur adipisicing elit</strong></p>
        </div>
        <div class="col-lg-4 col-md-4 mt-3">
                <iframe width="250" height="200" src="https://www.youtube.com/embed/jyTNhT67ZyY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <p><strong>Lorem ipsum dolor sit amet consectetur adipisicing elit</strong></p>
        </div>
        <div class="col-lg-4 col-md-4 mt-3">
                <iframe width="250" height="200" src="https://www.youtube.com/embed/5K7OGSsWlzU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <p><strong>Lorem ipsum dolor sit amet consectetur adipisicing elit</strong></p>
        </div>
    </div>
</div>
@endsection